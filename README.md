# simple_latex_template

A simple custom LaTeX presentation template

To use this LaTeX beamer template you will need to create a folder called 'texmf' in your Home directory, then one called 'tex' in that, then one called 'latex'. You then place the 'texsx folder in the 'latex' folder. Most LaTeX editors will pick up custom style files from this folder structure. You will see that the theme is called in the texsx-example.tex file with the \usetheme() call.
